package Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import Main.Calcul;

class CalculTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testSomme() {
		
		Calcul calcul = new Calcul(); 
		
		if (calcul.somme(2, 3) != 5)
			fail("Faux pour 2 entiers positifs");
		
		if (calcul.somme(-2, -3) != -5)
			fail("Faux pour 2 entiers n�gatifs");
		
		if (calcul.somme(-2, 3) != 1)
			fail("Faux pour 2 entiers de signe diff�rent");
		
		if (calcul.somme(0, 3) != 3)
			fail("Faux pour x null");
		
		if (calcul.somme(3, 0) != 3)
			fail("Faux pour y null");
		
		if (calcul.somme(0, 0) != 0)
			fail("Faux pour x et y null");
		
	}
	
	@Test
	void testMultiplication() {
		
		Calcul calcul = new Calcul(); 
		
		if (calcul.multiplication(2, 3) != 6)
			fail("Faux pour 2 entiers positifs");
		
		if (calcul.multiplication(-2, -3) != 6)
			fail("Faux pour 2 entiers n�gatifs");
		
		if (calcul.multiplication(-2, 3) != -6)
			fail("Faux pour 2 entiers de signe diff�rent");
		
		if (calcul.multiplication(0, 3) != 0)
			fail("Faux pour x null");
		
		if (calcul.multiplication(3, 0) != 0)
			fail("Faux pour y null");
		
		if (calcul.multiplication(0, 0) != 0)
			fail("Faux pour x et y null");
		
	}

	@Test
	void testDivision() {
		
		Calcul calcul = new Calcul(); 
		
		assertFalse(calcul.division(6, 3) == 3, "2 entiers positifs");
		assertEquals(calcul.division(-6, -3),2, "2 entiers negatifs");
		assertNotNull("2 entiers de signe differents", calcul.division(-6, 3));
		assertTrue(calcul.division(0, 3) == 0, "entier x = 0");
		
		Throwable e = null;
		try {
			calcul.division(2, 0);
		}
		catch (Throwable ex) {
			e = ex;
		}
		assertTrue(e instanceof ArithmeticException);
		e = null;
		
		try {
			calcul.division(0, 0);
		}
		catch (Throwable ex) {
			e = ex;
		}
		assertTrue(e instanceof ArithmeticException);
		
	}
	
	@Test
	void testSoustraction() {
		
		Calcul calcul = new Calcul(); 
		
		assertFalse(calcul.soustraction(6, 3) != 3, "2 entiers positifs");
		assertEquals(calcul.soustraction(-6, -3),-3, "2 entiers negatifs");
		assertNotNull("2 entiers de signe differents", calcul.soustraction(-6, 3));
		assertTrue(calcul.soustraction(0, 3) == -3, "entier x = 0");
		
	}
	
	@Test
	void testEssais_FG() {
		
		Calcul calcul = new Calcul(); 
		
		for(int i = -10; i <= 10; i++) {		
			for(int j = -10; j <= 10; j++) {
				assertEquals(calcul.soustraction(i, j),(i-j), "double boucle soustraction");	
				assertEquals(calcul.somme(i, j),(i+j), "double boucle somme");	
				assertEquals(calcul.multiplication(i, j),(i*j), "double boucle multiplication");	
				if (j != 0) assertEquals(calcul.division(i, j),(i/j), "double boucle division");	
			}	
		}
	}

}
